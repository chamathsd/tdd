package launchcode;

import org.junit.Test;
import static org.junit.Assert.*;
import org.hamcrest.collection.IsMapContaining;


public class TestUrl {

    private final Url basicUrl = new Url("https://launchcode.org:8080/learn?action=edit&status=okay#heading");
    private final Url mixedUrl = new Url("HtTpS://lAuNcHcOdE.oRg/LeArN");
    private final Url subdomainUrl = new Url("ftp://qux.bar.foo.example.com");

    @Test
    public void testProtocolSetAfterConstruction() {
        assertEquals("https", basicUrl.getProtocol());
    }

    @Test
    public void testDomainSetAfterConstruction() {
        assertEquals("launchcode.org", basicUrl.getDomain());
    }

    @Test
    public void testPortSetAfterConstruction() {
        assertEquals(8080, basicUrl.getPort());
    }

    @Test
    public void testPathSetAfterConstruction() {
        assertEquals("/learn", basicUrl.getPath());
    }

    @Test
    public void testQueryStringsSetAfterConstruction() {
        assertThat(basicUrl.getQueryMap(), IsMapContaining.hasEntry("action", "edit"));
    }

    @Test
    public void testMultipleQueryStringsSetAfterConstruction() {
        final Url multipleQueryUrl = new Url("https://test.org/?hello=world&test=good");
        assertThat(multipleQueryUrl.getQueryMap(), IsMapContaining.hasEntry("hello", "world"));
        assertThat(multipleQueryUrl.getQueryMap(), IsMapContaining.hasEntry("test", "good"));
    }

    @Test
    public void testFragmentSetAfterConstruction() {
        assertEquals("heading", basicUrl.getFragment());
    }

    @Test
    public void testMixedUrlReturnsLowercaseProtocol() {
        assertEquals("https", mixedUrl.getProtocol());
    }

    @Test
    public void testMixedUrlReturnsLowercaseDomain() {
        assertEquals("launchcode.org", mixedUrl.getDomain());
    }

    @Test
    public void testMixedUrlReturnsLowercasePath() {
        assertEquals("/learn", mixedUrl.getPath());
    }

    @Test
    public void testSubdomainsAreRemoved() {
        assertEquals("example.com", subdomainUrl.getDomain());
    }

    @Test
    public void testAcceptsIPAddress() {
        final Url ipUrl = new Url("https://127.0.0.1:8080/");
        assertEquals("127.0.0.1", ipUrl.getDomain());
    }

    @Test
    public void testNoPathInAddress() {
        assertEquals("/", subdomainUrl.getPath());
    }

    @Test
    public void testUrlToString() {
        assertEquals("ftp://example.com/", subdomainUrl.toString());
        assertEquals("https://launchcode.org:8080/learn?action=edit&status=okay#heading", basicUrl.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadProtocolCausesException() {
        final Url badProtocolUrl = new Url("telnet://www.google.com");
        fail("Invalid protocol should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadDomainCausesException() {
        final Url badDomainUrl = new Url("https://bad?domain.com");
        fail("Invalid domain should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadPortCausesException() {
        final Url badPortUrl = new Url("https://www.google.com:8d0e/");
        fail("Invalid port should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadQueryStringCausesException() {
        final Url badQueryStringUrl = new Url("https://www.google.com/?hello=world=test#heading");
        fail("Invalid query string should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyProtocolCausesException() {
        final Url emptyProtocolUrl = new Url("launchcode.org");
        fail("Empty protocol should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomainCausesException() {
        final Url emptyDomainUrl = new Url("https://");
        fail("Empty domain should throw IllegalArgumentException");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadURLOrderCausesException() {
        final Url badOrderUrl = new Url("https://launchcode.org/#heading?action=edit");
        fail("Url components being out of order should throw IllegalArgumentException");
    }

}
