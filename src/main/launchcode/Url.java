package launchcode;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.HashMap;


public class Url {

    private final List<String> validProtocols = Arrays.asList("http", "https", "ftp", "file");

    private String protocol;
    private String domain;
    private String path;
    private int port;
    private Map<String, String> queryMap;
    private String fragment;

    private static void skipTokens(StringTokenizer tokenizer, int num) throws IllegalArgumentException {
        for (int i = 0; i < num; i++) {
            if (tokenizer.hasMoreTokens()) {
                tokenizer.nextToken();
            } else {
                throw new IllegalArgumentException("Invalid URL");
            }
        }
    }

    public Url(String address) throws IllegalArgumentException {

        StringTokenizer st = new StringTokenizer(address, ":/?&#", true);

        // Set defaults
        path = "";
        port = 0;
        queryMap = new HashMap<>();
        fragment = "";

        // Handle protocol
        protocol = st.nextToken().toLowerCase();

        if (!validProtocols.contains(protocol)) {
            throw new IllegalArgumentException("Invalid protocol provided");
        }

        skipTokens(st, 3);

        // Handle domain
        String domainToken;
        if (st.hasMoreTokens()) {
            domainToken = st.nextToken().toLowerCase();
        } else {
            throw new IllegalArgumentException("No domain after protocol");
        }

        if (domainToken.matches(".*[^a-zA-Z0-9._-].*")) {
            throw new IllegalArgumentException("Invalid characters in domain");
        }

        String[] splitDomain = domainToken.split("\\.");
        int domainComponents = splitDomain.length;
        if (domainComponents < 2) {
            // Reject hostname without a TLD or bad IP address
            throw new IllegalArgumentException("Not enough components in domain");
        }

        if (domainToken.matches(".*[a-zA-Z_-].*")) {
            // If we have any letters in the domain, assume hostname, remove subdomains
            domain = splitDomain[domainComponents - 2] + "." + splitDomain[domainComponents - 1];
        } else {
            // Assume IP address, don't eliminate leading period separators
            domain = String.join(".", splitDomain);
        }

        boolean canHandlePort = true,
                canHandlePath = true,
                canHandleQuery = true,
                canHandleQueryAdds = false,
                canHandleFragment = true;

        // Handle remainder of URL (optional elements), enforcing order they must appear in
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            switch (token) {
                case ":":
                    // Handle port
                    if (canHandlePort && st.hasMoreTokens()) {
                        String portToken = st.nextToken();
                        if (portToken.matches(".*[^0-9].*")) {
                            throw new IllegalArgumentException("Invalid characters in port");
                        } else {
                            port = Integer.parseInt(portToken);
                        }
                    } else {
                        throw new IllegalArgumentException("Malformed URL parameters");
                    }
                    canHandlePort = false;
                    break;
                case "/":
                    // Handle path
                    if (canHandlePath) {
                        path += "/";
                    } else {
                        throw new IllegalArgumentException("Malformed URL parameters");
                    }
                    canHandlePort = false;
                    break;
                case "?":
                    if (!canHandleQuery) throw new IllegalArgumentException("Malformed URL parameters");
                    canHandlePort = false;
                    canHandlePath = false;
                    canHandleQuery = false;
                    canHandleQueryAdds = true;
                case "&":
                    if (canHandleQueryAdds && st.hasMoreTokens()) {
                        String queryToken = st.nextToken();
                        String[] query = queryToken.split("=");
                        if (query.length == 2) {
                            queryMap.put(query[0], query[1]);
                        } else {
                            throw new IllegalArgumentException("Malformed URL parameters");
                        }
                    } else {
                        throw new IllegalArgumentException("Malformed URL parameters");
                    }
                    break;
                case "#":
                    if (canHandleFragment && st.hasMoreTokens()) {
                        fragment = st.nextToken();
                    } else {
                        throw new IllegalArgumentException("Malformed URL parameters");
                    }
                    canHandlePort = false;
                    canHandlePath = false;
                    canHandleQuery = false;
                    canHandleQueryAdds = false;
                    canHandleFragment = false;
                    break;
                default:
                    path += token.toLowerCase();
                    break;
            }
        }

        if (path == "") {
            path = "/";
        }

    }

    public String getProtocol() {
        return protocol;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public int getPort() { return port; }

    public Map<String, String> getQueryMap() { return queryMap; }

    public String getFragment() { return fragment; }

    public String toString() {
        String result = getProtocol() + "://" + getDomain()
                + (port > 0 ? ":" + getPort() : "")
                + getPath();
        Map<String, String> queries = getQueryMap();

        if (queries.size() != 0) {
            boolean first = true;
            for (Map.Entry<String, String> entry : queries.entrySet()) {
                result += (first ? "?" : "&");
                result += entry.getKey() + "=" + entry.getValue();
                first = false;
            }
        }

        if (!getFragment().equals("")) {
            result += "#" + getFragment();
        }

        return result;
    }
}
